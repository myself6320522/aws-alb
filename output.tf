# output "public_key" {
#   value = tls_private_key.rsa-4096-test.public_key_pem
#   sensitive = true
# }

# # 导出生成的密钥
# output "private_key" {
#   value = tls_private_key.rsa-4096-test.private_key_pem
#   sensitive = true
# }

# output "instance_ips" {
#   value = aws_instance.server.public_ip
# }

output "alb_dns_name" {
  value = aws_lb.web_alb.dns_name
  description = "The DNS name of the application load balancer"
}